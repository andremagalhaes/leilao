﻿using LeilaoOnline.Models;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Data.Entity;

namespace LeilaoOnline.Controllers
{
    [RoutePrefix("api/leilao")]
    public class LeilaoApiController : ApiController
    {
        private ApplicationDbContext _db = new ApplicationDbContext();

        [Authorize(Roles = "Admin, Usuario")]
        [Route("todos")]
        [ResponseType(typeof(JsonSerializer))]
        [AcceptVerbs("GET", "POST")]
        [HttpGet]
        public IHttpActionResult ObterLeiloes()
        {
            List<Leilao> leiloes = _db.Leiloes.OrderByDescending(x => x.DataFim).ToList();

            foreach (Leilao leilao in leiloes)
                leilao.Lances = _db.LancesLeilao.Where(x => x.LeilaoId == leilao.Id).OrderByDescending(x => x.Data).ToList();

            return Ok(leiloes);
        }

        [AllowAnonymous]
        [Route("count")]
        [ResponseType(typeof(JsonSerializer))]
        [AcceptVerbs("GET", "POST")]
        [HttpGet]
        public IHttpActionResult ObterQtdLeiloes(string tipo)
        {
            int leiloes = 0;

            switch (tipo)
            {
                case "ativo":
                    leiloes = _db.Leiloes.Where(x => x.StatusId == new Guid("4df2f05b-1a14-42f4-91ad-c6c13a0bfea3")).Count();
                    break;
                case "cancelado":
                    leiloes = _db.Leiloes.Where(x => x.StatusId == new Guid("88433ebb-b7a6-478f-8a50-f3b6b2eb06c0")).Count();
                    break;
                case "encerrado":
                    leiloes = _db.Leiloes.Where(x => x.StatusId == new Guid("583b53fb-8837-4e5a-85ff-6b28521562e2")).Count();
                    break;
                case "pendente":
                    leiloes = _db.Leiloes.Where(x => x.StatusId == new Guid("c839da73-87b3-4684-ade0-6a3170d86f9c")).Count();
                    break;
                default:
                    leiloes = _db.Leiloes.Count();
                    break;
            }

            return Ok(new { qtd = leiloes });
        }

        [Authorize(Roles = "Admin, Usuario")]
        [Route("info")]
        [ResponseType(typeof(JsonSerializer))]
        [AcceptVerbs("GET", "POST")]
        [HttpGet]
        public IHttpActionResult ObterLeilao(Guid leilaoId)
        {
            Leilao leilao = _db.Leiloes.Find(leilaoId);

            if (leilao == null)
                return Content(HttpStatusCode.BadRequest, "Leilão Inesistente!");

            leilao.Lances = _db.LancesLeilao.Where(x => x.LeilaoId == leilao.Id).OrderByDescending(x => x.Data).ToList();

            return Ok(leilao);
        }

        [Authorize(Roles = "Admin")]
        [Route("novo")]
        [ResponseType(typeof(JsonSerializer))]
        [AcceptVerbs("POST")]
        [HttpPost]
        public IHttpActionResult CriarLeilao(Leilao leilao)
        {
            if (!ModelState.IsValid)
                return Content(HttpStatusCode.BadRequest, ModelState);

            if (_db.Leiloes.Where(x => x.Nome == leilao.Nome).FirstOrDefault() != null)
                return Content(HttpStatusCode.BadRequest, "Já existe um leilão cadastrado com esse nome!");

            try
            {
                leilao.Id = Guid.NewGuid();
                leilao.StatusId = new Guid("c839da73-87b3-4684-ade0-6a3170d86f9c"); //Pendente

                _db.Leiloes.Add(leilao);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, "Erro ao cadastrar Leilão!");
            }

            return Ok(leilao);
        }

        [Authorize(Roles = "Admin")]
        [Route("atualiza")]
        [ResponseType(typeof(JsonSerializer))]
        [AcceptVerbs("POST")]
        [HttpPost]
        public IHttpActionResult AtualizaLeilao(Leilao leilao)
        {
            if (leilao.Id == null)
                return Content(HttpStatusCode.BadRequest, "Campo Id Inesistente!");

            if (_db.Leiloes.Where(x => x.Nome == leilao.Nome).FirstOrDefault() != null)
                return Content(HttpStatusCode.BadRequest, "Já existe um leilão cadastrado com esse nome!");

            Leilao leilaoNovo = _db.Leiloes.Find(leilao.Id);

            if (leilaoNovo.StatusId != new Guid("c839da73-87b3-4684-ade0-6a3170d86f9c")) // Verifica se Status é Pendente
                return Content(HttpStatusCode.BadRequest, "O Status não está mais Pendente, por isso não pode ser alterado!");

            if (leilao.Nome != null) leilaoNovo.Nome = leilao.Nome;
            if (leilao.DataAbertura != null) leilaoNovo.DataAbertura = leilao.DataAbertura;
            if (leilao.DataFim != null) leilaoNovo.DataFim = leilao.DataFim;
            if (leilao.Usado != null) leilaoNovo.Usado = leilao.Usado;
            if (leilao.ValorInicial != null) leilaoNovo.ValorInicial = leilao.ValorInicial;
            if (leilao.ResponsavelId != null) leilaoNovo.ResponsavelId = leilao.ResponsavelId;

            try
            {
                _db.Entry(leilaoNovo).State = EntityState.Modified;
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, "Erro ao atualizar Leilão!");
            }

            return Ok(leilao);
        }

        [Authorize(Roles = "Admin")]
        [Route("ativa")]
        [ResponseType(typeof(JsonSerializer))]
        [AcceptVerbs("POST")]
        [HttpPost]
        public IHttpActionResult AtivaLeilao(Guid leilaoId)
        {
            return AlteraStatusLeilao(leilaoId, "ativar");
        }

        [Authorize(Roles = "Admin")]
        [Route("encerra")]
        [ResponseType(typeof(JsonSerializer))]
        [AcceptVerbs("POST")]
        [HttpPost]
        public IHttpActionResult EncerraLeilao(Guid leilaoId)
        {
            return AlteraStatusLeilao(leilaoId, "encerrar");
        }

        [Authorize(Roles = "Admin")]
        [Route("cancela")]
        [ResponseType(typeof(JsonSerializer))]
        [AcceptVerbs("POST")]
        [HttpPost]
        public IHttpActionResult CancelaLeilao(Guid leilaoId)
        {
            return AlteraStatusLeilao(leilaoId, "cancelar");
        }

        [Authorize(Roles = "Admin")]
        [Route("deleta")]
        [ResponseType(typeof(JsonSerializer))]
        [AcceptVerbs("DELETE")]
        [HttpPost]
        public IHttpActionResult DeletaLelao(Guid leilaoId)
        {
            Leilao leilao = _db.Leiloes.Find(leilaoId);

            if (leilao == null)
                return Content(HttpStatusCode.BadRequest, "Leilão Inesistente!");

            try
            {
                _db.Entry(leilao).State = EntityState.Deleted;
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, "Erro ao Excluir Leilão!");
            }

            return Ok(new { id = leilao.Id, message = "Leilão Excluído com sucesso!" });
        }

        public IHttpActionResult AlteraStatusLeilao(Guid leilaoId, string acao)
        {
            bool error = false;
            Leilao leilao = _db.Leiloes.Find(leilaoId);

            if (leilao == null)
                return Content(HttpStatusCode.BadRequest, "Leilão Inesistente!");

            StatusLeilao statusLeilao = _db.StatusesLeilao.Where(x => x.Id == leilao.StatusId).FirstOrDefault();

            switch (acao)
            {
                case "ativar":

                    if (leilao.StatusId == new Guid("c839da73-87b3-4684-ade0-6a3170d86f9c")) // Pendente
                        leilao.StatusId = new Guid("4df2f05b-1a14-42f4-91ad-c6c13a0bfea3"); //Ativo
                    else
                        error = true;

                    break;
                case "encerrar":

                    if (leilao.StatusId == new Guid("c839da73-87b3-4684-ade0-6a3170d86f9c")
                         || leilao.StatusId == new Guid("4df2f05b-1a14-42f4-91ad-c6c13a0bfea3")) //Pendente ou Ativo
                        leilao.StatusId = new Guid("583b53fb-8837-4e5a-85ff-6b28521562e2"); //Encerrado
                    else
                        error = true;

                    break;
                case "cancelar":

                    if (leilao.StatusId == new Guid("c839da73-87b3-4684-ade0-6a3170d86f9c")
                         || leilao.StatusId == new Guid("4df2f05b-1a14-42f4-91ad-c6c13a0bfea3")) //Pendente ou Ativo
                        leilao.StatusId = new Guid("88433ebb-b7a6-478f-8a50-f3b6b2eb06c0"); //Cancelado
                    else
                        error = true;

                    break;
                default:
                    break;
            }

            if (error)
                return Content(HttpStatusCode.BadRequest, new { messege = $"O Leilão está {statusLeilao.Nome}!" });

            try
            {
                _db.Entry(leilao).State = EntityState.Modified;
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, $"Erro ao {acao} Leilão!");
            }

            return Ok(new { id = leilao.Id, message = "Leilão atualizado com sucesso!" });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _db.Dispose();

            base.Dispose(disposing);
        }
    }
}