﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeilaoOnline.Models
{
    [Table("Status")]
    public class Status
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        public string Nome { get; set; }

    }
}