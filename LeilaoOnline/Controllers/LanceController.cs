﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using LeilaoOnline.Models;
using LeilaoOnline.Services;
using Microsoft.AspNet.Identity;

namespace LeilaoOnline.Controllers
{

    [Authorize(Roles = "Usuario")]
    public class LanceController : Controller
    {
        private ApplicationDbContext _db = new ApplicationDbContext();

        public ActionResult Index()
        {
            var lancesLeilao = _db.LancesLeilao.Include(l => l.Leilao).Include(l => l.Usuario);

            return View(lancesLeilao.ToList());
        }

        [Authorize(Roles = "Admin, Usuario")]
        public ActionResult List(Guid leilaoId)
        {
            var lancesLeilao = _db.LancesLeilao.Where(x => x.LeilaoId == leilaoId).OrderByDescending(x => x.Data).Include(l => l.Leilao).Include(l => l.Usuario);

            ViewBag.LeilaoId = leilaoId;

            return View(lancesLeilao.ToList());
        }

        public ActionResult Details(Guid id)
        {
            LanceLeilao lanceLeilao = _db.LancesLeilao.Find(id);

            if (lanceLeilao == null)
                return HttpNotFound();

            return View(lanceLeilao);
        }

        public ActionResult Create(Guid leilaoId)
        {
            string userId = User.Identity.GetUserId();

            LanceLeilao lance = new LanceLeilao
            {
                LeilaoId = leilaoId,
                UsuarioId = _db.Usuarios.Where(x => x.UserId == userId).FirstOrDefault().Id
            };

            return View(lance);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LanceLeilao lanceLeilao)
        {
            Leilao leilao = _db.Leiloes.Find(lanceLeilao.LeilaoId);

            LanceLeilao lance = _db.LancesLeilao.Where(x => x.LeilaoId == lanceLeilao.LeilaoId)
                                                .OrderByDescending(x => x.Valor)
                                                .FirstOrDefault();

            if (lance != null && lance.Valor >= lanceLeilao.Valor)
                ModelState.AddModelError("ValorMininoError", $"Valor Inferior ao último lance! Último Lance: R$ {lance.Valor}");
            if (lance == null && leilao.ValorInicial >= lanceLeilao.Valor)
                ModelState.AddModelError("ValorMininoError", $"Valor Inferior ao valor inicial! Lance Inicial: R$ {leilao.ValorInicial}");

            if (ModelState.IsValid)
            {
                lanceLeilao.Id = Guid.NewGuid();
                lanceLeilao.Data = GlobalManager.GetDateTimeNow();

                _db.LancesLeilao.Add(lanceLeilao);
                _db.SaveChanges();

                return RedirectToAction("List", new { leilaoId = lanceLeilao.LeilaoId });
            }

            return View(lanceLeilao);
        }

        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LanceLeilao lanceLeilao = _db.LancesLeilao.Find(id);
            if (lanceLeilao == null)
            {
                return HttpNotFound();
            }
            ViewBag.LeilaoId = new SelectList(_db.Leiloes, "Id", "Nome", lanceLeilao.LeilaoId);
            ViewBag.UsuarioId = new SelectList(_db.Usuarios, "Id", "Nome", lanceLeilao.UsuarioId);
            return View(lanceLeilao);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,LeilaoId,UsuarioId,Valor,Data")] LanceLeilao lanceLeilao)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(lanceLeilao).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LeilaoId = new SelectList(_db.Leiloes, "Id", "Nome", lanceLeilao.LeilaoId);
            ViewBag.UsuarioId = new SelectList(_db.Usuarios, "Id", "Nome", lanceLeilao.UsuarioId);
            return View(lanceLeilao);
        }

        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LanceLeilao lanceLeilao = _db.LancesLeilao.Find(id);
            if (lanceLeilao == null)
            {
                return HttpNotFound();
            }
            return View(lanceLeilao);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            LanceLeilao lanceLeilao = _db.LancesLeilao.Find(id);
            _db.LancesLeilao.Remove(lanceLeilao);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
