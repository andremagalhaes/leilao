﻿using LeilaoOnline.Models;
using System;
using System.Threading.Tasks;

namespace PaoEmCasaDelivery.Services
{
    public class UsuarioManager : IDisposable
    {
        private ApplicationDbContext _db = new ApplicationDbContext();

        public virtual Task<Usuario> CreateAsync(Usuario usuario)
        {
            try
            {
                usuario.Id = Guid.NewGuid();
                usuario.StatusId = new Guid("48426b39-5539-4d69-8085-069096d96d3e"); //Ativo
                usuario.Cpf = usuario.Cpf.Trim().Replace(".", "").Replace("-", "");

                _db.Usuarios.Add(usuario);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                usuario = new Usuario();
            }

            return Task.FromResult(usuario);
        }

        #region IDisposable Support
        private bool disposedValue = false; // Para detectar chamadas redundantes

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _db.Dispose();
                }

                // TODO: liberar recursos não gerenciados (objetos não gerenciados) e substituir um finalizador abaixo.
                // TODO: definir campos grandes como nulos.
                disposedValue = true;
            }
        }

        // TODO: substituir um finalizador somente se Dispose(bool disposing) acima tiver o código para liberar recursos não gerenciados.
        // ~Usuario() {
        //   // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
        //   Dispose(false);
        // }

        // Código adicionado para implementar corretamente o padrão descartável.
        public void Dispose()
        {
            _db.Dispose();
            // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
            Dispose(true);
            // TODO: remover marca de comentário da linha a seguir se o finalizador for substituído acima.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}