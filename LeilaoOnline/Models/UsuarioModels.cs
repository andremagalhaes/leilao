﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeilaoOnline.Models
{
    [Table("Usuario")]
    public class Usuario
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Cpf { get; set; }

        [Required]
        public Guid StatusId { get; set; }

        [Required]
        public string UserId { get; set; }

        [NotMapped]
        public List<string> Roles { get; set; }

        public virtual Status Status { get; set; }
        public virtual ApplicationUser User { get; set; }

    }
}