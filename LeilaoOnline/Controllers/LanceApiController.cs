﻿using LeilaoOnline.Models;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using Newtonsoft.Json;
using System;
using System.Net;
using Microsoft.AspNet.Identity;
using LeilaoOnline.Services;

namespace LeilaoOnline.Controllers
{
    [RoutePrefix("api/lance")]
    public class LanceApiController : ApiController
    {
        private ApplicationDbContext _db = new ApplicationDbContext();

        [Authorize(Roles = "Admin, Usuario")]
        [Route("todos")]
        [ResponseType(typeof(JsonSerializer))]
        [AcceptVerbs("GET", "POST")]
        [HttpGet]
        public IHttpActionResult ObterLances(Guid leilaoId)
        {
            List<LanceLeilao> lances = _db.LancesLeilao.Where(x => x.LeilaoId == leilaoId).OrderByDescending(x => x.Data).ToList();

            if (lances.Count == 0)
                return Ok("Nenhum lance para esse Leilão!");

            return Ok(lances);
        }

        [AllowAnonymous]
        [Route("count")]
        [ResponseType(typeof(JsonSerializer))]
        [AcceptVerbs("GET", "POST")]
        [HttpGet]
        public IHttpActionResult ObterQtdLances()
        {
            int lances = _db.LancesLeilao.Count();

            return Ok(new { qtd = lances });
        }

        [Authorize(Roles = "Usuario")]
        [Route("novo")]
        [ResponseType(typeof(JsonSerializer))]
        [AcceptVerbs("GET", "POST")]
        [HttpGet]
        public IHttpActionResult GeraLances(Guid leilaoId, decimal valor)
        {
            Leilao leilao = _db.Leiloes.Find(leilaoId);

            if (leilao.StatusId != new Guid("4df2f05b-1a14-42f4-91ad-c6c13a0bfea3")) //Verifica se Leilão está ativo
                return Content(HttpStatusCode.BadRequest, $"Leilão não está ativo para Lances! Leilão está {leilao.Status.Nome}");

            if (leilao == null)
                return Content(HttpStatusCode.BadRequest, "Leilão Inesistente!");

            LanceLeilao ultimoLance = _db.LancesLeilao.Where(x => x.LeilaoId == leilaoId).OrderByDescending(x => x.Valor).FirstOrDefault();

            if (ultimoLance?.Valor >= valor)
                return Content(HttpStatusCode.BadRequest, $"Valor inferiorou igual ao último lance! Último lance: R$ {ultimoLance.Valor}");
            else if (ultimoLance == null && leilao.ValorInicial >= valor)
                return Content(HttpStatusCode.BadRequest, $"Valor inferior ou igual ao lance inicial! Lance inicial: R$ {leilao.ValorInicial}");

            string userId = User.Identity.GetUserId();
            Usuario usuario = _db.Usuarios.Where(x => x.UserId == userId).FirstOrDefault();

            if (usuario == null)
                return Content(HttpStatusCode.BadRequest, "Usuário Inesistente!");

            if (ultimoLance?.UsuarioId == usuario.Id)
                return Content(HttpStatusCode.BadRequest, $"Usuário já deu o últmo lance! Usuário: {usuario.Nome} - Lance: {ultimoLance.Valor}");

            LanceLeilao lance = new LanceLeilao()
            {
                Id = Guid.NewGuid(),
                LeilaoId = leilaoId,
                UsuarioId = usuario.Id,
                Data = GlobalManager.GetDateTimeNow(),
                Valor = valor
            };

            try
            {
                _db.LancesLeilao.Add(lance);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, "Erro ao cadastrar Lance!");
            }

            return Ok(lance);
        }
    }
}