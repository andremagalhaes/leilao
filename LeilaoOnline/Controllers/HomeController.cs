﻿using LeilaoOnline.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LeilaoOnline.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext _db = new ApplicationDbContext();

        public ActionResult Index()
        {
            ViewBag.Title = "Home";

            ViewBag.Lances = _db.LancesLeilao.Count();
            ViewBag.LeiLoesAtivo = _db.Leiloes.Where(x => x.StatusId == new Guid("4df2f05b-1a14-42f4-91ad-c6c13a0bfea3")).Count();
            ViewBag.LeiLoesFinalizado = _db.Leiloes.Where(x => x.StatusId == new Guid("583b53fb-8837-4e5a-85ff-6b28521562e2")).Count();
            ViewBag.UsuariosAtivo = _db.Usuarios.Where(x => x.StatusId == new Guid("48426b39-5539-4d69-8085-069096d96d3e")).Count();

            return View();
        }

        public ActionResult Login()
        {
            ViewBag.Title = "Login";

            return View();
        }

        public ActionResult Register()
        {
            ViewBag.Title = "Registrar";

            return View();
        }
    }
}
