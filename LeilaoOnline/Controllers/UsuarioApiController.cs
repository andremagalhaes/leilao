﻿using LeilaoOnline.Models;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Data.Entity;

namespace LeilaoOnline.Controllers
{
    [Authorize(Roles = "Admin")]
    [RoutePrefix("api/usuario")]
    public class UsuarioApiController : ApiController
    {
        private ApplicationDbContext _db = new ApplicationDbContext();

        [Route("todos")]
        [ResponseType(typeof(JsonSerializer))]
        [AcceptVerbs("GET", "POST")]
        [HttpGet]
        public IHttpActionResult ObterUsuarios()
        {
            List<Usuario> usuarios = _db.Usuarios.OrderBy(x => x.Nome).ToList();

            foreach (Usuario usuario in usuarios.Where(x => x.UserId != null))
                usuario.Roles = usuario.User.Roles.Select(x => x.RoleId).ToList();

            return Ok(usuarios);
        }

        [AllowAnonymous]
        [Route("count")]
        [ResponseType(typeof(JsonSerializer))]
        [AcceptVerbs("GET", "POST")]
        [HttpGet]
        public IHttpActionResult ObterQtdUsuarios(string tipo)
        {
            int usuarios = 0;

            switch (tipo)
            {
                case "ativo":
                    usuarios = _db.Usuarios.Where(x => x.StatusId == new Guid("48426b39-5539-4d69-8085-069096d96d3e")).Count();
                    break;
                case "inativo":
                    usuarios = _db.Leiloes.Where(x => x.StatusId == new Guid("692c63b9-a579-4f07-9127-11e859a896ad")).Count();
                    break;
                default:
                    usuarios = _db.Leiloes.Count();
                    break;
            }

            return Ok(new { qtd = usuarios });
        }

        [Route("info")]
        [ResponseType(typeof(JsonSerializer))]
        [AcceptVerbs("GET", "POST")]
        [HttpGet]
        public IHttpActionResult ObterUsuario(Guid usuarioId)
        {
            Usuario usuario = _db.Usuarios.Find(usuarioId);

            if (usuario == null)
                return Content(HttpStatusCode.BadRequest, "Usuário Inesistente!");

            usuario.Roles = usuario.User.Roles.Select(x => x.RoleId).ToList();

            return Ok(usuario);
        }

        [Route("desativa")]
        [ResponseType(typeof(JsonSerializer))]
        [AcceptVerbs("POST")]
        [HttpPost]
        public IHttpActionResult DesativaUsuario(Guid usuarioId)
        {
            Usuario usuario = _db.Usuarios.Find(usuarioId);

            if (usuario == null)
                return Content(HttpStatusCode.BadRequest, "Usuário Inválido!");

            if (usuario.StatusId == new Guid("692c63b9-a579-4f07-9127-11e859a896ad"))
                return Ok(new { messege = "Usuário já está Inativo!" });

            usuario.StatusId = new Guid("692c63b9-a579-4f07-9127-11e859a896ad"); //Inativo

            try
            {
                _db.Entry(usuario).State = EntityState.Modified;
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, "Erro ao Desativar Usuário!");
            }

            return Ok(new { id = usuario.Id, message = "Usuário Desativado com sucesso!" });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _db.Dispose();

            base.Dispose(disposing);
        }
    }
}