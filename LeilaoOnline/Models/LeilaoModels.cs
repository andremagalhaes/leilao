﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LeilaoOnline.Models
{
    [Table("Leilao")]
    public class Leilao
    {
        public Guid Id { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        public decimal ValorInicial { get; set; }

        [Required]
        public bool Usado { get; set; }

        [Required]
        public DateTime DataAbertura { get; set; }

        [Required]
        public DateTime DataFim { get; set; }

        [Required]
        public Guid ResponsavelId { get; set; }

        public Guid StatusId { get; set; }

        [NotMapped]
        public List<LanceLeilao> Lances { get; set; }

        public virtual Usuario Responsavel { get; set; }
        public virtual StatusLeilao Status { get; set; }
    }

    [Table("LanceLeilao")]
    public class LanceLeilao
    {
        public Guid Id { get; set; }

        [Required]
        public Guid LeilaoId { get; set; }

        public Guid UsuarioId { get; set; }

        [Required]
        public decimal Valor { get; set; }

        public DateTime Data { get; set; }

        public virtual Usuario Usuario { get; set; }
        public virtual Leilao Leilao { get; set; }
    }

    [Table("StatusLeilao")]
    public class StatusLeilao
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        public int Ordem { get; set; }
    }
}