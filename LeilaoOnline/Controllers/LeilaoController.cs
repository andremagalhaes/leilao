﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using LeilaoOnline.Models;

namespace LeilaoOnline.Controllers
{

    [Authorize]
    public class LeilaoController : Controller
    {
        private ApplicationDbContext _db = new ApplicationDbContext();

        [AllowAnonymous]
        public ActionResult Index()
        {
            var leiloes = _db.Leiloes.Include(l => l.Responsavel).Include(l => l.Status);

            return View(leiloes.ToList());
        }

        [Authorize(Roles = "Usuario")]
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Leilao leilao = _db.Leiloes.Find(id);
            if (leilao == null)
            {
                return HttpNotFound();
            }
            return View(leilao);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.ResponsavelId = new SelectList(_db.Usuarios, "Id", "Nome");
            ViewBag.StatusId = new SelectList(_db.StatusesLeilao, "Id", "Nome");

            return View();
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nome,ValorInicial,Usado,DataAbertura,DataFim,ResponsavelId,StatusId")] Leilao leilao)
        {
            if (ModelState.IsValid)
            {
                leilao.Id = Guid.NewGuid();

                _db.Leiloes.Add(leilao);
                _db.SaveChanges();

                return RedirectToAction("Index");
            }

            ViewBag.ResponsavelId = new SelectList(_db.Usuarios, "Id", "Nome", leilao.ResponsavelId);
            ViewBag.StatusId = new SelectList(_db.StatusesLeilao, "Id", "Nome", leilao.StatusId);
            return View(leilao);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Leilao leilao = _db.Leiloes.Find(id);
            if (leilao == null)
            {
                return HttpNotFound();
            }
            ViewBag.ResponsavelId = new SelectList(_db.Usuarios, "Id", "Nome", leilao.ResponsavelId);
            ViewBag.StatusId = new SelectList(_db.StatusesLeilao, "Id", "Nome", leilao.StatusId);
            return View(leilao);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nome,ValorInicial,Usado,DataAbertura,DataFim,ResponsavelId,StatusId")] Leilao leilao)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(leilao).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ResponsavelId = new SelectList(_db.Usuarios, "Id", "Nome", leilao.ResponsavelId);
            ViewBag.StatusId = new SelectList(_db.StatusesLeilao, "Id", "Nome", leilao.StatusId);
            return View(leilao);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Leilao leilao = _db.Leiloes.Find(id);
            if (leilao == null)
            {
                return HttpNotFound();
            }
            return View(leilao);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Leilao leilao = _db.Leiloes.Find(id);
            _db.Leiloes.Remove(leilao);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
