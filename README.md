**Git

* https://bitbucket.org/andremagalhaes/leilao/src

**WebSite

* andretotvs-001-site1.itempurl.com

* Acesso Admin: Email: admin@leilao.com.br - Senha: leilao123

* Acesso ao Banco: Server: SQL5034 - Base: DB_A43519_leilao - Usu�rio: DB_A43519_leilao_admin - Senha: leilao123

**Collection Api Postman

* https://www.getpostman.com/collections/4131fdc9d6be02b27c9d

**Ferramentas e Tecnologias Utilizadas

* Visual Studio
* .Net Framework 4.7
* Sql Server

**Apis

* http://andretotvs-001-site1.itempurl.com/api/Account/Register
	header
	{
		Content-Type: application/json
	}
	
	data
	{
		Email: _email_,
		Password: _password_,
		ConfirmPassword: _confirm_password_,
		Nome: _nome_,
		Cpf: _cpf_
	}
	
* http://andretotvs-001-site1.itempurl.com/Token
	header
	{
		Content-Type: application/json
	}	
	data
	{
		grant_type: password,
		userName: _user_name_,
		password: _password_
	}
	
* http://andretotvs-001-site1.itempurl.com/api/Account/Logout
	header
	{
		Authorization: bearer _your_token_
	}

* http://andretotvs-001-site1.itempurl.com/api/Account/UserInfo
	header
	{
		Authorization: bearer _your_token_
	}

* http://andretotvs-001-site1.itempurl.com/api/usuario/todos
	header
	{
		Content-Type: application/json,
		Authorization: bearer _your_token_
	}

* http://andretotvs-001-site1.itempurl.com/api/usuario/count?tipo=ativo
	params
	{
		tipo: _tipe_ (ativo, inativo)
	}

* http://andretotvs-001-site1.itempurl.com/api/usuario/info?usuarioId=c1dc0ca4-b8de-4f3f-8739-c8bc3bafe55e
	params
	{
		usuarioId: _usuarioId_
	}
	header
	{
		Content-Type: application/json,
		Authorization: bearer _your_token_
	}

* http://andretotvs-001-site1.itempurl.com/api/usuario/desativa?usuarioId=c1dc0ca4-b8de-4f3f-8739-c8bc3bafe55e
	params
	{
		usuarioId: _usuarioId_
	}	
	header
	{
		Content-Type: application/json,
		Authorization: bearer _your_token_
	}

* http://andretotvs-001-site1.itempurl.com/api/leilao/todos
	header
	{
		Content-Type: application/json,
		Authorization: bearer _your_token_
	}

* http://andretotvs-001-site1.itempurl.com/api/leilao/count?tipo=ativo
	params
	{
		tipo: _tipe_ (ativo, pendente, cancelado, encerrado)
	}	
	header
	{
		Content-Type: application/json
	}
		
* http://andretotvs-001-site1.itempurl.com/api/leilao/info?leilaoId=e16d0167-9bb8-40fa-87f2-6189a27dac64
	params
	{
		leilaoId: _leilaoId_
	}	
	header
	{
		Content-Type: application/json,
		Authorization: bearer _your_token_
	}
		
* http://andretotvs-001-site1.itempurl.com/api/leilao/novo
	header
	{
		Content-Type: application/json,
		Authorization: bearer _your_token_
	}
	data
	{
		Nome: _nome_,
		ValorInicial: _valorInicial_,
		Usado: _usado_,
		DataAbertura: _dataAbertura_,
		DataFim: _dataFim_,
		ResponsavelId: _responsavelId_
	}


* http://andretotvs-001-site1.itempurl.com/api/leilao/atualiza
	header
	{
		Content-Type: application/json,
		Authorization: bearer _your_token_
	}
	data
	{
		Id: _id_,
		Nome: _nome_,
		ValorInicial: _valorInicial_,
		Usado: _usado_,
		DataAbertura: _dataAbertura_,
		DataFim: _dataFim_,
		ResponsavelId: _responsavelId_
	}

* http://andretotvs-001-site1.itempurl.com/api/leilao/ativa?leilaoId=e16d0167-9bb8-40fa-87f2-6189a27dac64
	params
	{
		leilaoId: _leilaoId_
	}	
	header
	{
		Content-Type: application/json,
		Authorization: bearer _your_token_
	}
		
* http://andretotvs-001-site1.itempurl.com/api/leilao/cancela?leilaoId=3e1f1f3c-0163-4048-95f2-b889e7ef1c77
	params
	{
		leilaoId: _leilaoId_
	}	
	header
	{
		Content-Type: application/json,
		Authorization: bearer _your_token_
	}

* http://andretotvs-001-site1.itempurl.com/api/leilao/encerra?leilaoId=3e1f1f3c-0163-4048-95f2-b889e7ef1c77
	params
	{
		leilaoId: _leilaoId_
	}	
	header
	{
		Content-Type: application/json,
		Authorization: bearer _your_token_
	}
	
* http://andretotvs-001-site1.itempurl.com/api/lance/todos?leilaoId=e16d0167-9bb8-40fa-87f2-6189a27dac64
	params
	{
		leilaoId: _leilaoId_
	}	
	header
	{
		Content-Type: application/json,
		Authorization: bearer _your_token_
	}
	
* http://andretotvs-001-site1.itempurl.com/api/lance/novo?leilaoId=e16d0167-9bb8-40fa-87f2-6189a27dac64&valor=82572.56
	params
	{
		leilaoId: _leilaoId_,
		valor: _valor_
	}	
	header
	{
		Content-Type: application/json,
		Authorization: bearer _your_token_
	}

* http://andretotvs-001-site1.itempurl.com/api/lance/count
	header
	{
		Content-Type: application/json
	}